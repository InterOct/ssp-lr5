﻿namespace LR5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.fromNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fromAddressTextBox = new System.Windows.Forms.TextBox();
            this.toNameTextBox = new System.Windows.Forms.TextBox();
            this.toAddressTextBox = new System.Windows.Forms.TextBox();
            this.subjectTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bodyTextBox = new System.Windows.Forms.TextBox();
            this.encrypt = new System.Windows.Forms.Button();
            this.decrypt = new System.Windows.Forms.Button();
            this.submit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fromName
            // 
            this.fromNameTextBox.Location = new System.Drawing.Point(64, 6);
            this.fromNameTextBox.Name = "fromNameTextBox";
            this.fromNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.fromNameTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "От кого";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Кому";
            // 
            // fromAddress
            // 
            this.fromAddressTextBox.Location = new System.Drawing.Point(170, 6);
            this.fromAddressTextBox.Name = "fromAddressTextBox";
            this.fromAddressTextBox.Size = new System.Drawing.Size(102, 20);
            this.fromAddressTextBox.TabIndex = 4;
            // 
            // toName
            // 
            this.toNameTextBox.Location = new System.Drawing.Point(64, 32);
            this.toNameTextBox.Name = "toNameTextBox";
            this.toNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.toNameTextBox.TabIndex = 5;
            // 
            // toAddress
            // 
            this.toAddressTextBox.Location = new System.Drawing.Point(170, 32);
            this.toAddressTextBox.Name = "toAddressTextBox";
            this.toAddressTextBox.Size = new System.Drawing.Size(102, 20);
            this.toAddressTextBox.TabIndex = 6;
            // 
            // subject
            // 
            this.subjectTextBox.Location = new System.Drawing.Point(64, 58);
            this.subjectTextBox.Name = "subjectTextBox";
            this.subjectTextBox.Size = new System.Drawing.Size(208, 20);
            this.subjectTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Тема";
            // 
            // body
            // 
            this.bodyTextBox.Location = new System.Drawing.Point(12, 84);
            this.bodyTextBox.Multiline = true;
            this.bodyTextBox.Name = "bodyTextBox";
            this.bodyTextBox.Size = new System.Drawing.Size(260, 106);
            this.bodyTextBox.TabIndex = 9;
            // 
            // encrypt
            // 
            this.encrypt.Location = new System.Drawing.Point(12, 196);
            this.encrypt.Name = "encrypt";
            this.encrypt.Size = new System.Drawing.Size(127, 23);
            this.encrypt.TabIndex = 10;
            this.encrypt.Text = "Зашифровать";
            this.encrypt.UseVisualStyleBackColor = true;
            this.encrypt.Click += new System.EventHandler(this.encrypt_Click);
            // 
            // decrypt
            // 
            this.decrypt.Location = new System.Drawing.Point(145, 196);
            this.decrypt.Name = "decrypt";
            this.decrypt.Size = new System.Drawing.Size(126, 23);
            this.decrypt.TabIndex = 11;
            this.decrypt.Text = "Расшифровать";
            this.decrypt.UseVisualStyleBackColor = true;
            this.decrypt.Click += new System.EventHandler(this.decrypt_Click);
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(12, 226);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(260, 23);
            this.submit.TabIndex = 12;
            this.submit.Text = "Отправить";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.decrypt);
            this.Controls.Add(this.encrypt);
            this.Controls.Add(this.bodyTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.subjectTextBox);
            this.Controls.Add(this.toAddressTextBox);
            this.Controls.Add(this.toNameTextBox);
            this.Controls.Add(this.fromAddressTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fromNameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "LR5 (Email client)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox fromNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fromAddressTextBox;
        private System.Windows.Forms.TextBox toNameTextBox;
        private System.Windows.Forms.TextBox toAddressTextBox;
        private System.Windows.Forms.TextBox subjectTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox bodyTextBox;
        private System.Windows.Forms.Button encrypt;
        private System.Windows.Forms.Button decrypt;
        private System.Windows.Forms.Button submit;
    }
}

