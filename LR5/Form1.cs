﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace LR5
{
    public partial class Form1 : Form
    {
        private const string Password = "1uALjZEZhWMoiSfk5uAO";

        private static readonly MailAddress FromAddress = new MailAddress("ssp@irl.by", "Egor Usoltcev");

        public Form1()
        {
            InitializeComponent();
            fromAddressTextBox.Text = FromAddress.Address;
            fromAddressTextBox.Enabled = false;
            fromNameTextBox.Text = FromAddress.DisplayName;
            fromNameTextBox.Enabled = false;
        }

        private void submit_Click(object sender, EventArgs e)
        {
            var toAddress = new MailAddress(toAddressTextBox.Text, toNameTextBox.Text);
            var subject = subjectTextBox.Text;
            var body = bodyTextBox.Text;
            var smtpClient = new SmtpClient
            {
                Host = "smtp.yandex.ru",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(FromAddress.Address, Password),
                Timeout = 10000
            };
            try
            {
                submit.Enabled = false;
                using (var message = new MailMessage(FromAddress, toAddress) {Subject = subject, Body = body})
                {
                    smtpClient.Send(message);
                }
            }
            finally
            {
                submit.Enabled = true;
            }
        }

        private void encrypt_Click(object sender, EventArgs e)
        {
            bodyTextBox.Text = Convert.ToBase64String(Encoding.UTF8.GetBytes(bodyTextBox.Text));
        }

        private void decrypt_Click(object sender, EventArgs e)
        {
            bodyTextBox.Text = Encoding.UTF8.GetString(Convert.FromBase64String(bodyTextBox.Text));
        }
    }
}